---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of computer science at the University of British Columbia.


## Research Interests

I am interested in computational complexity theory and design of algorithms. Over the years, the problems I’ve worked on come from many areas, including bioinformatics, hardware verification, and combinatorial auctions. Much of my current work focuses on models (such as Chemical Reaction Networks) and algorithms for computing with molecules, as well as methods for predicting DNA kinetics.

## Selected Publications

{% bibliography --file selected.bib %}
